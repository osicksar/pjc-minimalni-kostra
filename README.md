# Minimální kostra grafu
Semestrální práce do předmětu PJC

## Zadání:
Minimální kostry mají široké využití - od návrhů tras elektrického vedení, přes vytváření routovacích tabulek v počítačových sítích až po shlukování (clustering) bodů v analýze dat či detekce útvarů v počítačovém vidění. Doporučujeme použít Borůvkův algoritmus. Na výstupu programu bude seznam vrcholů minimální kostry a její cena nebo přímo popis celého grafu pro Graphviz, s barevně odlišenými vrcholy v kostře.

## Postup řešení:
Program hledá minimální kostru grafu pomocí doporučeného Borůvkova algoritmu. O tomto algoritmu více zde: https://www.algoritmy.net/article/1396/Boruvkuv-algoritmus

## Vstupy:
Vstup je načítán ze souboru. Program přijímá jeden parametr, což je cesta k tomuto souboru. 
V repozitáři jsou 3 vzorové vstupy, které byly také použity pro následné měření a sice graph1.txt, graph2.txt a graph3.txt.
V souboru se nachází popis grafu ve formátu:
```
<počet-vrcholů> <počet-hran>
0 <počet hran z vrcholu 0> <cílový vrchol 1> <délka 1. hrany> <cílový vrchol 2> <délka 2. hrany> ...
1 <počet hran z vrcholu 1> <cílový vrchol 1> <délka 1. hrany> <cílový vrchol 2> <délka 2. hrany> ...
...
```

## Výstupy:
Výstupem programu je kromě výpisu zadaného grafu také řešení, které obsahuje seznam použitých hran v minimální kostře a celkovou délkou této kostry v podobě:
```
<vrchol, ze kterého 1. hrana vychází> -> <vrchol, kam 1. hrana směřuje> (cost<délka 1. hrany>)
<vrchol, ze kterého 2. hrana vychází> -> <vrchol, kam 2. hrana směřuje> (cost<délka 2. hrany>)
...
Cost: <celková délka minimální kostry grafu>
Needed <jak dlouho vyřešení daného grafu trvalo> to finish
```

## Měření:
Měření proběhlo na čtyřjádrovém procesoru pouze pro jednovláknovou implementaci.
```
Graf se 2 vrcholy a 2 hranami (viz graph1.txt): 1 ms
Graf se 4 vrcholy a 6 hramani (viz graph2.txt): 6 ms
Graf s 10 vrcholy a 17 hranami(viz graph3.txt): 28 ms
```
