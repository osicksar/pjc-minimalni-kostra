#include "component.h"


Components::Components(int vertexNum)
{
    compNum = vertexNum;
    for (int i = 0; i < vertexNum; i++)
    {
        vertexComponent.push_back(i);
        Component c;
        c.vertexes.push_back(Vertex(i));
        c.isValid = true;
        c.parent = i;

        comps.push_back(std::move(c));
    }
}

void Components::addEdge(int from, int to, int cost)
{
    comps[from].vertexes[0].edges.push_back(Edge(to, cost));
}

void Components::merge(int comp1, int comp2)
{
    for (auto& v : comps[comp2].vertexes)
    {
        vertexComponent[v.index] = comp1;
        comps[comp1].vertexes.push_back(std::move(v));
    }
    comps[comp2].isValid = false;
    comps[comp2].parent = comp1;
    --compNum;
}

int Components::smallestOutward(int comp, std::pair<int, int>& outEdge, int& cost)
{
    int minimum = std::numeric_limits<int>::max();
    int toComp = comp;
    for (auto& v : comps[comp].vertexes)
    {
        for (auto& e : v.edges)
        {
            if (e.cost < minimum && vertexComponent[e.to] != comp)
            {
                minimum = e.cost;
                toComp = vertexComponent[e.to];
                outEdge = std::make_pair(v.index, e.to);
                cost = e.cost;
            }
        }
    }
    return toComp;
}

void Components::oneBlueberryIteration(OutputEdges& res)
{
    std::vector<std::pair<int, int>> toMerge;

    for (int c1 = 0; c1 < (int)comps.size(); c1++)
    {
        if (comps[c1].isValid)
        {
            std::pair<int, int> outEdge;
            int cost;
            int c2 = smallestOutward(c1, outEdge, cost);
            toMerge.push_back(std::make_pair(c1, c2));

            if (outEdge.first > outEdge.second)
            {
                std::swap(outEdge.first, outEdge.second);
            }
            res[outEdge] = cost;
        }
    }

    for (auto& m : toMerge)
    {
        m.first = findParent(m.first);
        m.second = findParent(m.second);
        if (m.first != m.second)
        {
            merge(m.first, m.second);
        }
    }
}

int Components::findParent(int c)
{
    while (comps[c].parent != c)
    {
        c = comps[c].parent;
    }
    return c;
}

OutputEdges Components::wholeBlueberry()
{
    OutputEdges ret;
    while (compNum != 1)
    {
        oneBlueberryIteration(ret);
    }
    return ret;
}
