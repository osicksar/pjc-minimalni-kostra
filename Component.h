#ifndef COMPONENT_HPP_INCLUDED_
#define COMPONENT_HPP_INCLUDED_

#include <vector>
#include <list>
#include <map>
#include <limits>

using OutputEdges = std::map<std::pair<int, int>, int>;

struct Edge
{
    Edge(int t, int c) : to(t), cost(c) {}
    int to;
    int cost;
};

struct Vertex {
    Vertex(int i) : index(i) {}

    std::vector<Edge> edges;
    int index;
};

struct Component {
    std::vector<Vertex> vertexes;
    bool isValid;
    int parent;
};

class Components {
    std::vector<Component> comps;
    std::vector<int> vertexComponent;
    int compNum;

public:

    Components(int vertexNum);

    void addEdge(int from, int to, int cost);

    OutputEdges wholeBlueberry();

    void oneBlueberryIteration(OutputEdges& res);
    /**
    *Najde nejmen�� hranu dan� komponenty, Vrac� indexy vrchol�, kter� hrana propojuje a cenu hrany
    */
    int smallestOutward(int comp, std::pair<int, int>& outEdge, int& cost);

    /* Sjednot� dv� komponenty do jedn� */
    void merge(int comp1, int comp2);

    int findParent(int c);
};

#endif /* COMPONENT_HPP_INCLUDED_ */
