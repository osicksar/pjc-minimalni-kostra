#include <iostream>
#include <fstream>

#include <iomanip>
#include <iterator>
#include <algorithm>
#include <sstream>
#include <vector>
#include <chrono>

#include "component.h"
template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}


int main(int argc, char* argv[]) {
    auto start = std::chrono::high_resolution_clock::now();

    if (argc < 2)
    {
        throw std::runtime_error("Expecting argument");
    }

    std::ifstream f(argv[1], std::ios_base::in);

    int vertexNum, edgeNum;
    f >> vertexNum >> edgeNum;

    Components comps(vertexNum);

    std::cout << "Vertex num " << vertexNum << " edge " << edgeNum << std::endl;

    std::string line;
    getline(f, line);
    while (getline(f, line)) {
        std::istringstream ssLine(line);
        std::cout << line << std::endl;

        std::vector<int> inp;
        inp.insert(inp.end(), std::istream_iterator<int>(ssLine), std::istream_iterator<int>());

        int from = inp[0];
        int fromEdgeNum = inp[1];

        if ((int)(inp.size() - 2) != fromEdgeNum * 2)
        {
            std::cout << inp.size() << ", " << fromEdgeNum << std::endl;
            throw std::runtime_error("Bad input");
        }

        for (size_t i = 2; i < inp.size(); i += 2)
        {
            std::cout << from << " -> " << inp[i] << " cost " << inp[i + 1] << std::endl;
            comps.addEdge(from, inp[i], inp[i + 1]);
        }

    }

    f.close();

    auto res = comps.wholeBlueberry();

    int cost = 0;
    std::cout << "Result:" << std::endl;
    for (auto& e : res)
    {
        std::cout << e.first.first << " -> " << e.first.second << " (cost " << e.second << ")" << std::endl;
        cost += e.second;
    }
    std::cout << "Cost: " << cost << std::endl;



    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";

    return 0;
}
